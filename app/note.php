// public function store(Request $request){
	// 	dd($request->all());
	// 	if ($request->hasFile('images')){
			//$path = Storage::putFile('images', $request->file('image'));

			//$file = $request->file('image');
			// Lưu vào trong thư mục storage
			//$path = $file->store('images');

			// $file = $request->file('image');
            //$name = $file->getClientOriginalName();
   //          $file->move('images', $name);
			// dd($images);

			// $images = $request->file('images');
			// print_r($images);
	  //       foreach ($images as $image){
	  //           $image->store('images');
	  //       }
   //          dd('co file');
   //    		}else{
 //            dd('khong co file');
 //        }
	// }



//laanf 2
<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RequestProduct;
use App\Models\Product;
use App\Models\Order;
use App\Models\Category;
use App\Models\ProductImage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Gate;

class ProductController extends Controller
{
	public function index(Request $request){
		$products = Product::with('category:id,name');
		if($request->name) $products->where('name','like','%'.$request->name.'%');
		if($request->category_id) $products->where('category_id',$request->category_id);
		$products = $products->orderBy('updated_at','DESC')->paginate(4);
		$categories = $this->getCategories();
		return view('backend.products.index')->with([
			'products' => $products,
			'categories' =>$categories
		]);
	}

	public function showImages($id){
		$product = Product::find($id);
		$images = $product->images;
		return view('backend.products.show_image')->with([
			'images' => $images,
			'product' => $product
		]);
	}

	public function create(){
		// $user_create = Auth::user();//lấy user mà mh muốn kiểm tra là user hiện tại
		// if (Gate::allows('create-product', $user_create)) {
		// 	$categories = $this->getCategories();
		// 	return view('backend.products.create')->with([
		// 	'categories' => $categories
		// ]);
		// }
		$user = Auth::user();
		// dd($user->role);
		// dd($this->authorize('create',$user));
		$product = Product::find(1);
		if ($user->can('create',$product)) {
	    	$categories = $this->getCategories();
			return view('backend.products.create')->with([
			'categories' => $categories
			]);
		}else{
			dd(00000);
		}
	}

	public function store(RequestProduct $requestProduct){
		$product = new Product();
		$image = $requestProduct->file('image');
		$name_image = date('YmdHis')."_".$image->getClientOriginalName();
		Storage::disk('public')->putFileAs('images/product/main', $requestProduct->file('image'), $name_image);

		$user = Auth::user();
		$product->name = $requestProduct->get('name');
		$product->slug = str::slug($product->name);
		$product->description = $requestProduct->get('description');
		$product->content = $requestProduct->get('content');
		$product->amount = $requestProduct->get('amount');
		$product->image = $name_image;
		$product->category_id = $requestProduct->get('category_id');
		$product->user_id = Auth::user()->id;
		$product->origin_price = $requestProduct->get('origin_price');
		$product->sale_price = $requestProduct->get('sale_price');
		$product->hot = $requestProduct->get('hot') == 'on' ? 1 : 0;
		$product->discount_percent = $requestProduct->get('discount_percent');

		$product->save();
		$product_id = $product->id;

		$images = $requestProduct->file('images');
		if ($requestProduct->hasFile('images')){
			foreach ($images as $image) {
				$product_image = new ProductImage();
				if (isset($image)) {
					$name_product_image = date('YmdHis')."_".$image->getClientOriginalName();
					$product_image->name = $name_product_image;
					$product_image->path = 'storage/images/product/detail/'.$name_product_image;

					$product_image->product_id = $product_id;

					Storage::disk('public')->putFileAs('images/product/detail', $image, $name_product_image);
					$product_image->save();
				}
			}
		}
    	return redirect()->route('backend.product.index');
	}

	public function edit($id){
		$product = Product::findOrFail($id);
		$categories = Category::all();
    	return view('backend.products.edit')->with([
    		'product' => $product,
    		'categories' => $categories
    	]);
	}

	public function update(RequestProduct $requestProduct,$id){
		$product = Product::find($id);

		$image = $requestProduct->file('image');
		$name_image = date('YmdHis')."_".$image->getClientOriginalName();
		Storage::disk('public')->putFileAs('images/product/main',$image,$name_image);

		$product->name = $requestProduct->get('name');
		$product->slug = str::slug($product->name);
		$product->description = $requestProduct->get('description');
		$product->content = $requestProduct->get('content');
		$product->amount = $requestProduct->get('amount');
		$product->image = $name_image;
		$product->category_id = $requestProduct->get('category_id');
		$product->origin_price = $requestProduct->get('origin_price');
		$product->sale_price = $requestProduct->get('sale_price');
		$product->hot = $requestProduct->get('hot') == 'on' ? 1 : 0;
		$product->discount_percent = $requestProduct->get('discount_percent');

		$product->save();
		$product_id = $product->id;

		$images = $requestProduct->file('images');
		if ($requestProduct->hasFile('images')){
			foreach ($images as $image) {
				$product_image = new ProductImage();
				if (isset($image)) {
					$name_product_image = date('YmdHis')."_".$image->getClientOriginalName();
					$product_image->name = $name_product_image;
					$product_image->path = 'storage/images/product/detail/'.$name_product_image;

					$product_image->product_id = $product_id;

					Storage::disk('public')->putFileAs('images/product/detail', $image, $name_product_image);
					$product_image->save();
				}
			}
		}
    	return redirect()->route('backend.product.index');
	}

	public function destroy($id){
		$product = Product::findOrFail($id);
		Storage::disk('public')->delete('images/product/main/'.$product->image);
		$product_images = ProductImage::where('product_id',$product->id)->get();
		$id_product_images = array();
		foreach ($product_images as $product_image) {
			Storage::disk('public')->delete('images/product/detail/'.$product_image->name);
			$id_product_images[] = $product_image->id;
		}
		$product->delete();
		ProductImage::destroy($id_product_images);
		return redirect()->route('backend.product.index');
	}

	public function editStatus($id){
    	$product = Product::find($id);
    	if($product->status==1){
    		$product->status = 0;
    	}else{
    		$product->status = 1;
    	}
    	$product->save();
    	return redirect()->back();
    }

	public function getCategories(){
		return Category::all();
	}
}
